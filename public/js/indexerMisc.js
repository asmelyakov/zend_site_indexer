'use strict';

var app = angular.module("indexerMisc",['ui.bootstrap'],null);//TODO: wtf null...

app.controller("dialogCtrl",function($scope,$http,$dialog){


    var t = '<div class="modal-header">'+
        '<h3>Rename Section</h3>'+
        '</div>'+
        '<div class="modal-body">'+
        '<p>Enter new section name: <input ng-model="result" /></p>'+
        '</div>'+
        '<div class="modal-footer">'+
        '<button ng-click="close(result)" class="btn btn-primary" >Rename</button>'+
        '</div>';

    $scope.opts = {
        backdrop: true,
        keyboard: true,
        backdropClick: true,
        template:  t, // OR: templateUrl: 'path/to/view.html',
        controller: 'DialogController'
    };
    $scope.renameDialog = function(sect){
        var d = $dialog.dialog($scope.opts);
        d.open().then(function(result){
            if(result)
            {
                sect.title = result;
            }
        });
    };
    $scope.shareDialog = function(){
        var title = 'Share that url for your friends';
        var msg = 'http://localhost'+$scope.cv.shortlink;
        var btns = [{result:'mail', label: 'Send by e-mail'}, {result:'ok', label: 'OK', cssClass: 'btn-primary'}];

        $dialog.messageBox(title, msg, btns)
            .open()
            .then(function(result){
                if(result == 'mail'){
                    var mailmsg = {link:$scope.cv.shortlink, to: $scope.cv.basic.email};
                    $http({method:"GET",url:"sendmail",params:{json: angular.toJson(mailmsg)}})
                        .success(function(data, status, headers, config) {
                            $scope.addAlert("Email sended to "+$scope.cv.basic.email,'success');
                        }).error(function(data, status, headers, config) {
                            $scope.data = {message:status};
                        });
                }
            });
    };

});
// the dialog is injected in the specified controller
function DialogController($scope, dialog){
    $scope.close = function(result){
        dialog.close(result);
    };
};