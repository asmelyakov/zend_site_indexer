<?php
/**
 * Created by JetBrains PhpStorm.
 * User: istrelnikov
 * Date: 9/19/13
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application\Model;

use Zend\Dom\Query;

class PageSaverParser {
    public static function getPage($link){
        require_once('url_to_absolute.php');

        $page = array();
        if(PageSaverParser::retrieve_remote_file_size($link)>1024*1024){ // big files
            return null;
        }

        //$page['html'] = Crawler::get_url_data($link);
        require_once('Casper/Casperjs.php');
        $casperJS = new \Casperjs('casperjs');
        $result = json_decode($casperJS->getHtml($link));
        $page['html'] = $result->html;
        if(!$page['html']){
            return null;
        }
        $page['content'] = $result->plain;
        $page['title'] = $result->title;
        /* Get the MIME type and character set */
        preg_match( '@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s+charset=([^\s"]+))?@i',
            $page['html'], $matches );
        if ( isset( $matches[3] ) )
            $initialEncoding = $matches[3];

        if(isset($initialEncoding)){
            if( $initialEncoding != 'UTF-8' ){
                $page['html'] = preg_replace('/<meta(.+)charset(.+)>/i','<meta http-equiv="content-type" content="text/html; charset=utf-8">', $page['html']);
                $page['html'] = mb_convert_encoding($page['html'],'UTF-8',$initialEncoding);
                file_put_contents('charsets.txt',$link.' - !ENCODED!',FILE_APPEND);
                }
        }else{
                //TODO: good search for html5 charset ->curl charset -> default 'latin1'
            $charset = mb_detect_encoding($page['html']);
            if($charset!='UTF-8'){
                $page['html'] = mb_convert_encoding($page['html'],'UTF-8',$charset);
                file_put_contents('charsets.txt',$link.' - !ENCODED!',FILE_APPEND);
            }

            $page['html'] = '<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $page['html'];
        }

        /*** saving inner js ***/
        preg_match_all("/<script[^>]*>(.*)<.*script>/Uis",
            $page['html'], $innerJS );
        $page['html'] = preg_replace("/<script[^>]*>(.*)<.*script>/Uis",'PUT_SCRIPT_HERE!!!',$page['html']);

        /*** a new dom object ***/
        $dom = new Query($page['html']);
        /*/*** plaintext to index ***/
       /* $page['content'] = '';
        foreach($dom->queryXpath("//text()[not(ancestor::script)][not(ancestor::style)][not(ancestor::noscript)][not(ancestor::form)]") as $text){
            $page['content'] .= $text->nodeValue . ' ';
        };*/
        /*/*** title ***/
       /* $title_tag = $dom->queryXpath('//title');
        foreach($title_tag as $tag){
            $page['title'] = $tag->childNodes->item(0)->nodeValue;
        }*/

        $a_tags = $dom->queryXpath('//a');
        /*** links ***/
        $links = array();
        foreach ($a_tags as $tag){
            $uri = Crawler::prepareUri($tag->getAttribute('href'));

            $uri = url_to_absolute($link,$uri);
            $links[] = $uri;
            $tag->setAttribute('href',Crawler::link_to_local($uri));
        }
        $page['html'] = $a_tags->getDocument()->saveHTML();
        $dom = new Query($page['html']);
        $frame_tags = $dom->queryXpath('//iframe');
        foreach ($frame_tags as $tag){
            $uri = Crawler::prepareUri($tag->getAttribute('src'));

            $uri = url_to_absolute($link,$uri);
            $links[] = $uri;
            $tag->setAttribute('src',Crawler::link_to_local($uri));
        }
        $page['html'] = $frame_tags->getDocument()->saveHTML();
        $dom = new Query($page['html']);

        $page['links'] = $links;
        /*** css ***///
        $link_tags = $dom->queryXpath('//link');

        foreach ($link_tags as $tag){
            if($tag->getAttribute('href') && ($tag->getAttribute('rel')=='stylesheet' || stristr($tag->getAttribute('rel'),'icon'))){

                $uri = Crawler::prepareUri($tag->getAttribute('href'));
                $uri = url_to_absolute($link,$uri);
                $css[] = $uri;
                $local = Crawler::uri_to_local($uri,'css',$link);
                $tag->setAttribute('href',$local['path'].'/'.$local['name']);
            }
        }
        $page['html'] = $link_tags->getDocument()->saveHTML();


        $dom = new Query($page['html']);
        $page['css'] = $css;
        /*** images ***/
        $img_tags = $dom->queryXpath('//img');
        $images = array();
        foreach ($img_tags as $tag){
            if($tag->getAttribute('src')){
                $uri = Crawler::prepareUri($tag->getAttribute('src'));
                $uri = url_to_absolute($link,$uri);
                $local = Crawler::uri_to_local($uri,'image',$link);
                $tag->setAttribute('src',$local['path'].'/'.$local['name']);
                $images[] = $uri;
            }
        }
        $page['html'] = $img_tags->getDocument()->saveHTML();

        $page['images'] = $images;

        $scripts = array();
        foreach($innerJS[0] as $js){
            $uri = Crawler::matchProperty($js,"<script","</script>","src");
            $uri = url_to_absolute($link,$uri);
            $scripts[] = $uri;
            $local = Crawler::uri_to_local($uri,'script',$link);
            $js = Crawler::replaceProperty($js,"<script","</script>","src",$local['path'].'/'.$local['name'],1);
            $page['html']= preg_replace("/PUT_SCRIPT_HERE!!!/",$js,$page['html'],1);
        }
        $page['scripts'] = $scripts;
        return $page;
    }

    public static function prepareUri($url){
        $uri = str_replace(array('\\"','\\\'','\'','"'),'',$url);
        if(substr($uri, 0, 2) === '//'){
            $uri = 'http:'.$uri;
        }
        return $uri;
    }

    private static function retrieve_remote_file_size($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        $data = curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);
        return $size;
    }

    private static function get_url_data($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36');
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public static function uri_to_local($url,$type,$page_url){
        $ret['name'] = end(explode('/',$url));
        $ret['name'] = reset(explode('?', $ret['name']));
        $ret['name'] = reset(explode('%', $ret['name']));
        $ret['name'] ='attach';
        $parsed_page_url = parse_url($page_url);
        $ret['path'] = '/uploads/'.$parsed_page_url['host'].'/'.md5($page_url).'/'.$type.'/'.md5($url);
        return $ret;
    }
    public static function link_to_local($url){
        return 'preview?url='.md5($url);
    }

    public static function replaceProperty($data, $start, $end, $property, $alias, $limit = -1){
        //get blocks formed as: $start $property = "..." $end or $start $property = '...' $end
        $pattern = "!(".$start."){1}([^>]*?)".$property."\s*=\s*[\"\'](.*?)[\"\'](.*?)(".$end."){1}!s";
        $data = preg_replace($pattern, "{$start}\${2}{$property}=\"{$alias}\"\${4}{$end}", $data, $limit);
        return $data;
    }

    public static function matchProperty($data, $start, $end, $property){
        //get blocks formed as: $start $property = "..." $end or $start $property = '...' $end
        $pattern = "!(".$start."){1}([^>]*?)".$property."\s*=\s*[\"\'](.*?)[\"\'](.*?)(".$end."){1}!s";
        preg_match($pattern, $data, $data);
        return $data[3];
    }
}