<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\Crawler;
use Application\Model\IndexerDAO;
use Application\Model\IndexerModel;
use Application\Model\PageDAO;
use Application\Model\SitePageModel;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DOMDocument;

class ConsoleController extends AbstractActionController
{
    public function indexAction(){
        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        $indexer = new IndexerModel();
        $indexer->runQueryIndexing();
    }
    public function dropAction(){
        $dao = new IndexerDAO();
        $dao->forceSchemaReinstall();
    }
    public function repairAction(){
        $dao = new IndexerDAO();
        $dao->repairSchema();
    }
    public function testAction(){
        $indexer= new IndexerModel();
        $msg = $indexer->getStatus('http://example.com');
        return  $this->getResponse()->setContent($msg);
    }
}