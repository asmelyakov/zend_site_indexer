<?php

class Casperjs
{
    private $options = array(
        'load-images' => 'yes',
        'web-security' => 'no'
    );
    private $bin;
    public function __construct($bin)
    {
        $this->bin = $bin;
    }
    public function setOptions(array $options)
    {
        foreach ($options as $key => $v) {
            if (is_string($v)) {
                switch (strtolower($v)) {
                    case 'yes':
                    case 'true':
                        $v = true;
                        break;
                    case 'no':
                    case 'false':
                        $v = false;
                }
            }
            $v = (boolean) $v;
            if (!is_bool($v)) continue;

            $this->options[strtolower($key)] = ($v === true) ? 'yes' : 'no';
        }
    }

    public function getOptionsAsString(){
        $ret = '';
        foreach ($this->options as $key => $v) {
            $ret .= ' --'.$key.'='.$v;
        }
        return $ret;
    }

    public function execute($script, $argsString = ''){
        $bin = $this->bin;
        $options = $this->getOptionsAsString();

        return shell_exec("$bin$options $script $argsString");
    }
    public function getHtml($url){
        $argsString = '"'.$url.'"';
        $output = $this->execute(str_replace(" ", "\\ ", __DIR__).'/gethtml_indexing.js', $argsString);
        return $output;
    }

}
