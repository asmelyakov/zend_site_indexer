<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Application/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(
                    'Application\Entity' => 'application_entities'
                )
    ))),
    'router' => array(
        'routes' => array(

            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'indexer' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Indexer',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'actions' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[:action]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Indexer',
                                'action' =>''
                            ),
                        ),
                    ),
                ),
            ),
            'preview' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/preview',
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Preview',
                        'action'        => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Indexer' => 'Application\Controller\IndexerController',
            'Application\Controller\Console' => 'Application\Controller\ConsoleController',
            'Application\Controller\Preview' => 'Application\Controller\PreviewController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ZfcTwigViewStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'start-route' => array(
                    'options' => array(
                        'route'    => 'start',
                        'defaults' => array(
                            'controller' => 'Application\Controller\Console',
                            'action'     => 'index'
                        )
                    )
                ),
                'drop-route' => array(
                    'options' => array(
                        'route'    => 'drop-schema',
                        'defaults' => array(
                            'controller' => 'Application\Controller\Console',
                            'action'     => 'drop'
                        )
                    )
                ),
                'repair-route' => array(
                    'options' => array(
                        'route'    => 'repair',
                        'defaults' => array(
                            'controller' => 'Application\Controller\Console',
                            'action'     => 'repair'
                        )
                    )
                )

            ),
        ),
    ),
);
